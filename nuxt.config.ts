// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['@/assets/css/index.scss'],
  app: {
    baseURL: "/",
    head: {
      title: 'Entertainment Web App',
      htmlAttrs: {
        lang: 'en'
      },
      meta: [
        { name: 'description', content: 'Entertainment amazing site.' }
      ],
      link: [
        {
          rel: 'icon',
          type: 'image/png',
          sizes: '32x32',
          href: '/public/favicon.ico',
        },
        { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
        { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css2?family=Outfit:wght@300;500&display=swap',
        },
      ],
    }
  },
  vite: { css: { preprocessorOptions: { scss: { additionalData: '@use "@/assets/css/tokens.scss" as *;' } } } },
  modules: ['@pinia/nuxt']
})
