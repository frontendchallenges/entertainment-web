import { db } from '~~/server/db/media'

export default defineEventHandler((event) => db.media.filter(m => m.category === 'TV Series'))