import { Show, Image, Thumbnail } from "~~/model/show";

function toImage(m: any): Image {
    return m? new Image(m.small, m.medium, m.large) : new Image('', '', '')
}

function toThumbnail(m: any): Thumbnail {
    return new Thumbnail(toImage(m.trending), toImage(m.regular))
}

export function toShow(m: any): Show {
    return new Show(m.title, toThumbnail(m.thumbnail), m.year, m.category, m.rating, m.isBookmarked, m.isTrending)
}

export function toShows(media: any): Show[] {
    return (media || []).map((m: any) => toShow(m))
}
