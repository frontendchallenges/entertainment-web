import { db } from '~~/server/db/media'

export default defineEventHandler((event) => db.media)