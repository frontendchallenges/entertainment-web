import { users } from '~~/server/db/users'

export default defineEventHandler(async (event) => {
    const body = await readBody(event)
    const dbUser = users.users.filter(u => u.username === body.username)
    if (dbUser.length > 0 && dbUser[0].password === body.password) {
        return 'Ok'
    }
    throw createError({ statusCode: 401, statusMessage: 'The username or the password are not valid' })
})
