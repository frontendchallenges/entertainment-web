# Frontend Mentor - Interactive rating card component solution

This is a solution to the [Interactive rating card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/entertainment-web-app-J-UhgAW1X). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Navigate between Home, Movies, TV Series, and Bookmarked Shows pages
- Add/Remove bookmarks from all movies and TV series
- Search for relevant shows on all pages
- **Bonus**: Build this project as a full-stack application
- **Bonus**: If you're building a full-stack app, we provide authentication screen (sign-up/login) designs if you'd like to create an auth flow

### Expected Behaviour

- General
  - The navigation menu should be fixed to the left for larger screens. Use the "Desktop - Home" page in the design as a visual reference.
- Home
  - The trending section should scroll sideways to reveal other trending shows
  - Any search input should search through all shows (i.e. all movies and TV series)
- Movies
  - This page should only display shows with the "Movie" category
  - Any search input should search through all movies
- TV Series
  - This page should only display shows with the "TV Series" category
  - Any search input should search through all TV series
- Bookmarked Shows
  - This page should display all bookmarked shows from both categories
  - Any search input should search through all bookmarked shows

### Links

- Live Site URL: [Live Solution](https://frontendchallenges.gitlab.io/entertainment/) Unfortunately because there's a private api-key (.env.local file) this is included in .gitignore and this way the solution won't work except locally.
- Screenshot: [Entertainment](./screenshot.png)

## My process

1. Create project `yarn create vite`
   Add dependencies `yarn add --dev sass`
   Edit nuxt.config.ts to set the base publish URL
   ``app.baseURL: "/entertainment-web/" ,```   
   Also to add tokens.scss to the global css
2. Design the html with no style by now
  In Nuxt, we create the default layout and the pages. After that extract components    
  layout: default.vue:
    div.app
      AppHeader
      main
        slot
  pages:
    index
      section.media
        search-component
        section.trending
          h2.heading
          ShowsGrid
        ShowsSection
    movies
      section.movies
        search-component
        ShowsSection
    tv-series
      section.series
        search-component
        ShowsSection
    bookmarked
      section.bookmarked
        search-component
        ShowsSection
        ShowsSection
    login
      main
        div.sign-up--icon
          img
        article
          h1
          form
            div: input + span
            div: input + span
            button
          div(link to sign-up)
    sign-up
      main
        div.sign-up--icon
          img
        article
          h1
          form
            div: input + span
            div: input + span
            div: input + span
            button
          div(link to sign-up)
      
3. Add the minimum backend logic to make the site work (the UI is ugly but the site starts to work). This includes some backend work: nitro to provide endpoints
2. Create tokens.scss, colors.css, components/(links,nav,buttons,...) index to import the previous files
7. Notice and implement the active states.
8. Complement logic (e.g. login, sign-up, middleware to ensure authentication), limit cases, tests
9. Deploy to netlify, because this project uses nitro gitlab pages won't work

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Nuxt3 + Vue + Nitro
- Pinia
- Netlify

### Lessons

How to add a linter:
1. yarn add eslint --dev
2. Add a script in the package.json file "lint": "eslint",
3. yarn lint --init
4. Answer the questions.
5. Set the script in  package.json file "lint": "eslint ./src --fix",
6. Finally to format on Save in vscode, add the following to .vscode/settings.json
```
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
```

How to create a nuxt project

1. npx nuxi init <project-name>
2. code <project-name>
3. yarn install
4. yarn dev -o (to execute the project in dev mode)
5. Set the default layout (layouts/default.vue), add a pages/index.vue and set app.vue to call NuxtLayout and NuxtPage

How to add global styles in nuxt
1. Edit nuxt.config.ts to add 
```
vite: {    css: {      preprocessorOptions: {        scss: {          additionalData: '@use "@/assets/css/tokens.scss" as *;'        }      }    }  }
```

Locally preview production build:

```bash
npm run preview | yarn preview
```
*How to set pinia in nuxt*

1. yarn add pinia @pinia/nuxt
2. Create a stores/index.ts file
3. In nuxt.config.ts, add 
```
modules: [
    // ...
    '@pinia/nuxt',
  ],
```

Deploy to netlify
https://dev.to/sidhantpanda/self-hosted-gitlab-continuous-deployment-to-netlify-49lk, 
https://medium.com/js-dojo/deploying-vue-js-to-netlify-using-gitlab-continuous-integration-pipeline-1529a2bbf170

### Continued development

Implementing this project in Nuxt


### Useful resources
Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) 
- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)
- [Created a modal](https://www.digitalocean.com/community/tutorials/vuejs-vue-modal-component)
npx run lerna build

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.
