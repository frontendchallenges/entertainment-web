export class Field {
  value: string;
  valid: boolean;
  errMsg: string;
  constructor() {
    this.value = "";
    this.valid = true;
    this.errMsg = "";
  }
  validate(): boolean {
    this.valid = this.value !== "";
    this.errMsg = "Can't be empty";
    return this.valid;
  }
}

export class EmailField extends Field {
  validate(): boolean {
    this.valid =
      this.value !== "" &&
      /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(this.value);
    this.errMsg = "Not a valid email";
    return this.valid;
  }
}

export function useForm() {  
  const getInputClass = (f: Field): string => f.valid ? " " : "form__input--invalid";

  return { getInputClass }
}