import { Show } from "~~/model/show"
import { watchEffect, ref } from 'vue'

//Note filter must be a ref to be used in a watch
export function useSearch(filter: Ref<string>, shows: Show[], heading: string) {  
  const filteredShows = ref(shows)
  const searchMessage = ref(heading)

  function search(): void {
    const cleanedFilter = (filter.value || '').trim().toLowerCase()
    if (cleanedFilter) {
      filteredShows.value = shows.filter((s) => s.title.toLowerCase().indexOf(cleanedFilter) > -1)
      searchMessage.value = 'Found ' + filteredShows.value.length + ' results for "' + filter.value + '" in ' + heading
    } else {
      filteredShows.value = shows
      searchMessage.value = heading
    }
  }
  watchEffect(search)

  return { searchMessage, filteredShows }
}
