export class Show {
    title: string
    thumbnail: Thumbnail
    year: number
    category: string
    rating: string
    isBookmarked: boolean
    isTrending: boolean

    constructor(title: string, thumbnail: Thumbnail, year: number, category: string, rating: string, bookmarked: boolean, trending: boolean) {
        this.title = title
        this.thumbnail = thumbnail
        this.year = year
        this.category = category
        this.rating = rating
        this.isBookmarked = bookmarked
        this.isTrending = trending
    }
}

export class Thumbnail {
    trending: Image
    regular: Image

    constructor(trending: Image, regular: Image) {
        this.trending = trending
        this.regular = regular
    }
}

export class Image {
    small: string
    medium: string
    large: string

    constructor(small: string, medium: string, large: string) {
        this.small = small
        this.medium = medium
        this.large = large
    }
}
