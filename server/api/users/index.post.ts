import { users } from '~~/server/db/users'

export default defineEventHandler(async (event) => {
    const body = await readBody(event)
    if (!body || !body.username || !body.password) {
        throw createError({ statusCode: 400, statusMessage: 'The username and the password are required'})
    }
    const dbUser = users.users.filter(u => u.username === body.username)
    if (dbUser.length > 0) {
        throw createError({ statusCode: 400, statusMessage: 'The username already exists'})
    }
    users.users.push(body)
    return 'Ok'
})
