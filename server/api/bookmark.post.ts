import { db } from '~~/server/db/media'

export default defineEventHandler(async (event) => {
    const body = await readBody(event)
    if (!body || !body.title) {
        throw createError({ statusCode: 400, statusMessage: 'The title and bookmarked are required' + JSON.stringify(body) })
    } else {
        db.media.filter(m => m.title === body.title).forEach(m => m.isBookmarked = body.isBookmarked)
    }
    return 'Ok'
})
