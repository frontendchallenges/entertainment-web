import { useUsersStore } from '~~/stores/users'

export default defineNuxtRouteMiddleware((to, from) => {
    if (!isAuthenticated()) {
        return navigateTo('/login')
    }
})

function isAuthenticated(): boolean {
    const store = useUsersStore()
    return store.getUser.name != null
}
