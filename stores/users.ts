import { defineStore } from 'pinia'

export interface User {
  name: string
}

export const useUsersStore = defineStore('CartStore', {
  state: () => ({
    /** @type {User} */
    user: {} as User
  }),
  getters: {
    getUser (state): User {
      return state.user
    }    
  },
  actions: {
    logIn (email: string) {
      this.user.name = email
    },
    logOut () {
      this.user.name = ''
    }
  }
})
